//build in version 1.0.0
//file version 1.0.1
//main entry point for electron

const {app, BrowserWindow, nativeImage} = require("electron"),
path = require("path"),
url = require("url"),
fs = require("fs");

const screenResolutions = {
    height: JSON.parse(fs.readFileSync("electron_res/resolution.json", "utf8")).height,
    width: JSON.parse(fs.readFileSync("electron_res/resolution.json", "utf8")).width
}

let win,
icon = nativeImage.createFromPath(path.join(__dirname, "electron_res/static/img/logo.ico"));

const createPreLoadWindow = () => {
    win = new BrowserWindow({
        height: 150,
        width: 400,
        transparent: false,
        frame: false,
        icon: icon,
        alwaysOnTop: true,
        resizable: false,
    });

    win.loadURL(url.format({
        pathname: path.join(__dirname, "electron_res/templates/app_on_start_preloading.html"),
        protocol: 'file:',
        slashes: true
    }));

    win.on("closed", () => {
        win = null;
    });
}

app.on("ready", createPreLoadWindow);

app.on("window-all-closed", () => {
    app.quit();
});