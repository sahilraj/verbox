#build in version 1.0.0
#file version 1.0.0
#used to get the users screen resolutions and write it to the resolutions.json file

import json
import ctypes

user32 = ctypes.windll.user32

height = user32.GetSystemMetrics(1)
width = user32.GetSystemMetrics(0)

j_data = {}

j_data["height"] = height
j_data["width"] = width

return_json = json.dumps(j_data)

with open("resolution.json", "a") as my_file:
    my_file.write(return_json)