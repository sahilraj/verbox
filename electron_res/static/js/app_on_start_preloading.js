const $ = require("jquery");
const exec = require("child_process").exec;
const path = require("path");

const execute = (command, callback) => {
    exec(command, (error, stdout, stderr) => {
        callback(error, stdout, stderr);
    });
}

$("document").ready(() => {
    setTimeout(() => {
        alert("current directory: " + __dirname);
    }, 1000);
});